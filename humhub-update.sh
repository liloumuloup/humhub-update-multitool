#!/usr/bin/env bash
# Shell script for updating easily Humhub
# /!\ Warning : tested only for releases available through the website and not the git based installation ones

set -e

# Vars :
# WEBROOT : Webroot folder (default : /var/www)
# HUMHUB_ROOT : Humhub root folder (default : humhub)
# WEB_USER : User (default : www-data)
# WEB_GROUP : Group (default : www-data)
# DEST_VERSION : Destination version
# PHP_PATH : Usefull when you have multiple versions of PHP on your server (default : php)

# Init vars with default values
WEBROOT="/var/www"
HUMHUB_ROOT="humhub"
WEB_USER="www-data"
WEB_GROUP="www-data"
PHP_PATH="/usr/bin/php"

# Prints help usage for this script
usage() {
  echo "$0 -w|--webroot WEBROOT -H|--humhub HUMHUB_ROOT -u|--user WEB_USER -g|--group WEB_GROUP -v|--version DEST_VERSION -p|--php-path PHP_PATH"
}

# Check which user launched the script to avoid right problems
check_user() {
  SCRIPT_USER=$(whoami)
  if [ "${SCRIPT_USER}" != "${WEB_USER}" ]; then
    echo "Script not launched as ${WEB_USER} !"
    echo "Exiting..."
    exit 1
  fi
}

# Get all parameters
get_parameters() {
    while (( $# )); do
      case $1 in
          -w|--webroot)
          shift
          WEBROOT="${1}"
          ;;
          -H|--humhub)
          shift
          HUMHUB_ROOT="${1}"
          ;;
          -u|--user)
          shift
          WEB_USER="${1}"
          ;;
          -g|--group)
          shift
          WEB_GROUP="${1}"
          ;;
          -v|--version)
          shift
          DEST_VERSION="${1}"
          ;;
          -p|--php-path)
          shift
          PHP_PATH="${1}"
          ;;
          -h|--help)
          usage
          exit
          ;;
          *)
          echo "Parameter ${1} invalid !"
          exit 1
          ;;
      esac
    shift
    done
}

check_parameters() {
  # Validate that folders exists
  if [[ ! -d "${WEBROOT}" ]]; then
    echo "ERROR : Webroot path ${WEBROOT} doesn't exists !"
    exit 1
  fi
  if [[ ! -w "${WEBROOT}" ]]; then
    echo "ERROR : Webroot path ${WEBROOT} not writable by ${WEB_USER} !"
    exit 1
  fi

  cd "${WEBROOT}"
  if [[ ! -d "${HUMHUB_ROOT}" ]]; then
    echo "ERROR : Humhub path ${HUMHUB_ROOT} doesn't exists !"
    exit 1
  fi

  # Define working paths from verified ones
  HUMHUB_FULL_PATH="${WEBROOT}/${HUMHUB_ROOT}"
  HUMHUB_BACKUP_PATH="${WEBROOT}/${HUMHUB_ROOT}.backup"

  # Check user has been defined properly
  if [[ -z "${WEB_USER}" ]]; then
    echo "ERROR : WEB_USER not defined !"
    exit 1
  fi

  if [[ -z "${WEB_GROUP}" ]]; then
    echo "ERROR : WEB_GROUP not defined !"
    exit 1
  fi

  # Check given version is correct
  if [[ ! "${DEST_VERSION}" =~ ^[0-9][\.]([0-9]{2})[\.][0-9](-beta[\.][0-9])?$ ]] || [[ -z "${DEST_VERSION}" ]]; then
    echo "ERROR : Version ${DEST_VERSION} invalid !"
    exit 1
  fi

  # Check php is available
  if [[ ! -x "${PHP_PATH}" ]]; then
    echo "ERROR : PHP path ${PHP_PATH} invalid !"
    exit 1
  fi
}

# Warns to stop apache or put maintenance mode before updating
# shellcheck disable=SC2128
warning_begin_update() {
  echo "You are about to start the update of Humhub using these parameters:"
  echo "Webroot folder : ${WEBROOT}"
  echo "Humhub root folder : ${HUMHUB_ROOT}"
  echo "User : ${WEB_USER}"
  echo "Group : ${WEB_GROUP}"
  echo "Destination version : ${DEST_VERSION}"
  echo "PHP path : ${PHP_PATH}"

  read -p "Did you stopped Apache or put the website in maintenance mode? (Y/y) " -n 1 -r
  echo
  if [[ ! ${REPLY} =~ ^[Yy]$ ]]
  then
      [[ "$0" = "${BASH_SOURCE}" ]] && exit 1 || return 1
  fi
}
# Warns to start apache or remove maintenance mode before updating
warning_end_update() {
  echo "Congratulations ! Humhub has been updated is ${DEST_VERSION} properly !"
  echo "Don't forget to restart Apache or remove the maintenance mode !"
}

# Backup & Delete your current HumHub installation
backup_files() {
  echo "Backuping ${HUMHUB_FULL_PATH}..."
  mv "${HUMHUB_FULL_PATH}" "${HUMHUB_BACKUP_PATH}"
  echo "Backup done."
}
remove_backup_files() {
  echo "You are about to delete the path ${HUMHUB_BACKUP_PATH}."
  read -p "Would you like to continue? (Y/y) " -n 1 -r
  echo

  if [[ $REPLY =~ ^[Yy]$ ]]
  then
    echo "Deleting backup..."
    #rm -rf ${HUMHUB_BACKUP_PATH}
    echo "Temporary disabled !"
    echo "Delete backup done."
  else
    echo "Backup path ${HUMHUB_BACKUP_PATH} not deleted."
  fi
}

#TODO : MySQL backup part
# Get database info + credentials to backup it
#get_database_info(){

#}
# Check if dump available from cli or if needed to be done elsewhere
#check_mysql_dump(){

#}
# Backup database
#backup_database(){
#    get_database_info
#    check_mysql_dump
#}

#TODO : Cron handling part
# Stop crons before updating
#stop_cron(){
#}
# Start crons after updating
#start_cron(){
#}

# Download and Extract new version
get_new_version() {
  cd "${WEBROOT}" || exit 1

  if [[ ! -f "humhub-${DEST_VERSION}.tar.gz" ]]; then
    wget "https://download.humhub.com/downloads/install/humhub-${DEST_VERSION}.tar.gz"
  fi
  curl -s -k "https://download.humhub.com/downloads/install/humhub-${DEST_VERSION}.tar.gz.sha256.txt" | cut -d ' ' -f1 | xargs -I {} bash -c "echo {} humhub-${DEST_VERSION}.tar.gz" | sha256sum  --check
  tar xvfz "humhub-${DEST_VERSION}.tar.gz"
  mv "humhub-${DEST_VERSION}" "${HUMHUB_ROOT}"
}


# Restore individual files
restore_previous_install() {
  cp -rfa "${HUMHUB_BACKUP_PATH}/uploads" "${HUMHUB_FULL_PATH}"
  cp -rfa "${HUMHUB_BACKUP_PATH}/protected/config" "${HUMHUB_FULL_PATH}/protected"
  cp -rfa "${HUMHUB_BACKUP_PATH}/protected/modules" "${HUMHUB_FULL_PATH}/protected"

  if [[ -f "${HUMHUB_BACKUP_PATH}/.htaccess" ]]; then
    cp -a "${HUMHUB_BACKUP_PATH}/.htaccess" "${HUMHUB_FULL_PATH}"
  fi
}

# Flush caches
flush_caches() {
  cd "${HUMHUB_FULL_PATH}/protected" || exit 1
  ${PHP_PATH} yii cache/flush-all
}

# Database migration
database_migration(){
  cd "${HUMHUB_FULL_PATH}/protected" || exit 1
  ${PHP_PATH} yii migrate/up --includeModuleMigrations=1
}

# Update installed modules
update_modules(){
  cd "${HUMHUB_FULL_PATH}/protected" || exit 1
  "${PHP_PATH}" yii module/update-all
}

# Fix permissions
fix_permissions(){
  chown -R "${WEB_USER}":"${WEB_GROUP}" "${HUMHUB_FULL_PATH}"
}

# Actions ToDo to update Humhub :
# 1) Stop apache / Put your website in maintenance mode
# 2) Stop cron
# 3) Backup & Delete your current HumHub installation
# 4) Download and Extract new version
# 5) Restore important files
# 6) Flush caches
# 7) Database migration
# 8) Update installed modules
# 9) Fix permissions
# 10) Start cron
# 11) Start apache / Remove your website in maintenance mode
get_parameters "$@"

check_parameters
check_user

warning_begin_update
#stop_cron

backup_files
#backup_database

get_new_version
restore_previous_install

flush_caches
database_migration
fix_permissions

#start_cron
warning_end_update

